package ai.ddo.userdetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class
 */
@SpringBootApplication
public class SpringBootUserRestServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootUserRestServiceApplication.class, args);
    }
}
