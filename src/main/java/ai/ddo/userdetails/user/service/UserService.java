package ai.ddo.userdetails.user.service;

import ai.ddo.userdetails.user.model.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    List<User> retrieveAllUsers();

    User retrieveUser(long id);

    void deleteUser(long id);

    ResponseEntity<Object> createUser(User user);

    ResponseEntity<Object> updateUser(User user, long id);
}
